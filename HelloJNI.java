public class HelloJNI {

    static {
        System.loadLibrary("hello");
    }

    public native void hello();

    public static void main(String[] args) {
        HelloJNI hello = new HelloJNI();
        hello.hello();
    }

}
